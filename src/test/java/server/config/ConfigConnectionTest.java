package server.config;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static server.config.ConfigConnection.getConnection;

public class ConfigConnectionTest {

    @Test
    public void testConnection() {
        assertNotNull(getConnection(), "Connection ok");
    }
}
