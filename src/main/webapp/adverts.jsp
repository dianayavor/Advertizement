<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="uk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
            integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
            integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/adverts.css">
    <title> List adverts </title>
    <style>
        .advert-list {
            padding-left: 200px;
            padding-right: 200px;
            padding-top: 30px;
            align-items: center;
        }

        .card {
            border-bottom: groove;
            border-right: groove;
            margin: 5px;
        }

        .btn-group {
            align-self: end;
            width: 200px;
        }

        .btn-add {
            justify-content: center;
        }

        .navbar-adverts {
            display: flex;
            margin-left: 100px;
        }

        .navbar-adverts-user-data {
            display: flex;
            margin-right: 70px;
        }

        .card-categories {
            float: right;
            padding: 7px 12px;
            background-color: #125843;
            color: #f8f8f8;
            border-radius: 16px;
            font-size: 12px;
            border-bottom: groove;
            border-right: groove;
            width: fit-content;
            margin: 7px;
        }

        .dateOfCreation {
            margin-top: 14px;
            margin-right: 10px;
            color: #8f8989;
            font-size: 12px;
            font-style: italic;
        }

        .expiration-date {
            margin-right: 10px;
            color: #8f8989;
            font-size: 12px;
            font-style: italic;
        }

        .date {
            display: flex;
            flex-direction: column;
        }

        .card-header {
            display: flex;
            justify-content: space-between;
        }

        .card-title {
            margin-top: 10px;
        }
    </style>
    <script>
        function deleteAdvert(id) {
            var result = confirm('Do you want to delete advert?');
            if (result) {
                var f = document.form;
                f.method = "get";
                f.action = '/delete?id=' + id;
                f.submit();
            } else {
                return false;
            }
        }
    </script>
</head>
<body>
<nav class="navbar navbar-dark bg-success justify-content-between">
    <div class="navbar-adverts">
        <c:if test="${requestScope.user.role == 'ADMIN'}">
            <a href="users" class="navbar-brand">Users</a>
        </c:if>
        <a href="adverts" class="navbar-brand">Adverts<span class="sr-only"></span></a>
        <c:if test="${requestScope.user.role == 'USER' || requestScope.user.role == 'ADMIN'}">
            <a href="my-adverts" class="navbar-brand">My adverts</a>
        </c:if>
    </div>
    <div class="navbar-adverts-user-data">
        <c:if test="${requestScope.user.role == 'USER' || requestScope.user.role == 'ADMIN'}">
            <a href="" class="navbar-brand">${requestScope.user.firstname} ${requestScope.user.lastname}</a>
            <a href="adverts/logout" class="navbar-brand">Log out</a>
        </c:if>
        <c:if test="${requestScope.user.role == 'GUEST'}">
            <a href="login" class="navbar-brand">Sign in</a>
        </c:if>
    </div>
</nav>

<div class="advert-list">
    <div class="card-container text-dark bg-light mb-3">
        <c:if test="${requestScope.user.role == 'USER' || requestScope.user.role == 'ADMIN'}">
            <form method="get" action="adverts/add">
                <div class="btn-add">
                    <button type="submit" class="btn btn-success" name="button-add-advert">New advert</button>
                </div>
            </form>
        </c:if>

        <c:forEach items="${requestScope.adverts}" var="advert">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">${advert.title}</h2>
                    <div class="date">
                        <h6 class="dateOfCreation">Date of creation: ${advert.dateOfCreation}</h6>
                        <h6 class="expiration-date">Expiration date: ${advert.dateOfEnd}</h6>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-categories">${advert.categories.name}</h5>
                    <p class="card-text" align="justify">${advert.description}</p>
                </div>
                <c:if test="${(requestScope.user.id == requestScope.advert.userDto.id && requestScope.user.role == 'USER') || requestScope.user.role == 'ADMIN'}">
                    <div class="btn-group" role="group">
                        <c:if test="${advert.active == true}">
                            <button type="submit" class="btn btn-success">enabled</button>
                        </c:if>
                        <c:if test="${advert.active == false}">
                            <button type="submit" class="btn btn-danger">disabled</button>
                        </c:if>
                        <c:if test="${requestScope.user.id == advert.userDto.id}">
                            <form action="adverts/edit" method="get">
                                <input type="hidden" name="advertId" value="${advert.id}">
                                <button type="submit" class="btn btn-light">Edit</button>
                            </form>
                        </c:if>
                        <form action="adverts/delete" method="get" onsubmit="return deleteAdvert(${advert.id});">
                            <input type="hidden" name="advertId" value="${advert.id}">
                            <button type="submit" class="btn btn-light">
                                Delete
                            </button>
                        </form>
                    </div>
                </c:if>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>