<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="uk">
<head>
    <title>User list</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
            integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
            integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
            crossorigin="anonymous"></script>
    <nav class="navbar navbar-dark bg-success" style="background-color: #b77e7d;">
        <div class="container-header">
            <a href="adverts" class="navbar-brand">Adverts</a>
        </div>
    </nav>
    <style>
        .container-header {
            margin-left: 100px;
        }

        .container {
            align-items: center;
            align-self: center;
            padding-right: 100px;
            padding-left: 100px;
            margin-top: 20px;
        }

        .btn-delete, .btn-ban, .btn-remove-ban {
            margin-left: 10px;
        }

        .btn-group {
            display: flex;
            justify-content: center;
            align-content: space-between;
        }
    </style>
    <script src="js/user.js"></script>
</head>
<body>
<div class="container">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">First name</th>
            <th scope="col">Last name</th>
            <th scope="col">Email</th>
            <th scope="col">Date of birth</th>
            <th scope="col">Active</th>
            <th scope="col">Role</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.users}" var="user">
        <tr>
            <td><c:out value="${user.firstname}"/></td>
            <td><c:out value="${user.lastname}"/></td>
            <td><c:out value="${user.email}"/></td>
            <td><c:out value="${user.dateOfBirthday}"/></td>
            <td><c:out value="${user.active}"/></td>
            <td><c:out value="${user.role}"/></td>
            <td>
                <div class="btn-group" role="group">
                    <div class="btn-edit">
                        <form method="get" action=users/edit>
                            <input type="hidden" name="id" value="${user.id}">
                            <button type="submit" class="btn btn-success">Edit</button>
                        </form>
                    </div>
                    <c:if test="${requestScope.user.role != 'ADMIN'}">
                        <div class="btn-ban">
                            <form method="get" action="users/delete" onsubmit="return deleteUser(${user.id});">
                                <input type="hidden" name="id" value="${user.id}">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                        <div class="btn-remove-ban">
                            <form method="get" action="users/ban" onsubmit="return ban(${user.id});">
                                <input type="hidden" name="id" value="${user.id}">
                                <button type="submit" class="btn btn-warning">Ban</button>
                            </form>
                        </div>
                        <div class="btn-delete">
                            <form method="get" action="users/remove-ban" onsubmit="return removeBan(${user.id});">
                                <input type="hidden" name="id" value="${user.id}">
                                <button type="submit" class="btn btn-success">Remove Ban</button>
                            </form>
                        </div>
                    </c:if>
                </div>
            </td>
            </c:forEach>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
