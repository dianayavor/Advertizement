<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="uk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
            integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
            integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/registration.css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <title>Sign up</title>
    <script src="js/validate-registration-form.js"></script>
</head>
<body>
<div class="registration-container">
    <h1 class="label-sign-up">Sign up</h1>
    <div class="form-registration">
        <div class="container">
            <form name="registrationForm" action="registration" method="post" id="registrationForm"
                  onsubmit="return validateRegistration();">
                <div class="form-group">
                    <label for="firstName" class="form-label">Please, enter your first name</label>
                    <input type="text" class="form-control" id="firstName" name="firstname"
                           value="<c:if test="${firstname != null}">${firstname}</c:if>">
                    <span class="error-firstName" id="errorFirstName"></span>
                </div>
                <div class="form-group">
                    <label for="lastName" class="form-label">Please, enter your last name</label>
                    <input type="text" class="form-control" id="lastName" name="lastname"
                           value="<c:if test="${lastname != null}">${lastname}</c:if>">
                    <span class="error-lastname" id="errorLastName"></span>
                </div>
                <div class="form-group">
                    <label for="emailRegistration" class="form-label">Please, enter your email address</label>
                    <input type="email" class="form-control" id="emailRegistration" name="email"
                           value="<c:if test="${email != null}">${email}</c:if>">
                    <span class="error-email" id="errorEmail"></span>
                </div>
                <div class="form-group">
                    <label for="dob" class="form-label">Please, choose your date of birthday</label>
                    <input type="date" class="form-control" id="dob" name="dob"
                           value="<c:if test="${dob != null}">${dob}</c:if>">
                    <span class="error-dob" id="errorDob"></span>
                </div>

                <div class="form-group">
                    <label for="password" class="form-label">Enter password</label>
                    <input type="password" class="form-control" id="password" name="password">
                    <span class="error-password" id="errorPassword"></span>
                </div>
                <div class="form-group">
                    <label for="repeatPassword" class="form-label">Repeat password</label>
                    <input type="password" class="form-control" id="repeatPassword" name="passwordRepeat">
                    <span class="error-password-repeat" id="errorPasswordRepeat"></span>
                </div>
                <div class="error-account-credentials" id="errorAccountCredentials">${requestScope.error}</div>
                <div class="btn-signup">
                    <button type="submit" value="Submit" class="btn btn-success">Sign up</button>
                </div>
                <div>
                    <p class="login-here">Already have an account?<a href="login" class="login-here-link">
                        Log in </a>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
