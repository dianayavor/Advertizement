<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="uk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
            integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
            integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/user.css">
    <style>
        .form-edit-advert {
            width: 600px;
            align-content: center;
        }

        .label-edit {
            align-content: center;
        }

        .category-group, .btn-edited {
            margin: 10px;
        }

        .form-check {
            width: fit-content;
            align-content: end;
        }

        .form-label {
            align-content: start;
        }
    </style>
    <title>Edit advert</title>
</head>
<body>
<div class="edit-container-advert" align="center">
    <h1 class="label-edit"><c:if test="${requestScope.advert.id == null}">Add advert</c:if></h1>
    <h1 class="label-edit"><c:if test="${requestScope.advert.id != null}">Edit advert</c:if></h1>

    <form class="form-edit-advert" method="post" action="edit">
        <input type="hidden" name="advertId"
               value="<c:if test="${requestScope.advert != null}">${requestScope.advert.id}</c:if>">
        <div class="form-group">
            <label for="title" class="form-label">Title</label>
            <input type="text" class="form-control" id="title" name="title"
                   value="<c:if test="${requestScope.advert != null}">${requestScope.advert.title}</c:if>">
        </div>

        <div class="form-group">
            <label for="description" class="form-label">Description</label>
            <div class="description-teaxarea">
                            <textarea rows="10" class="form-control" id="description"
                                      name="description"><c:if
                                    test="${requestScope.advert != null}">${requestScope.advert.description}</c:if></textarea>
            </div>
        </div>

        <label for="category-group">Category: </label>
        <select class="category-group" name="category" id="category-group">
            <c:forEach items="${requestScope.categories}" var="category">
                <option value="${category.id}">${category.name}</option>
            </c:forEach>
        </select>


<%--        selected="<c:if test="${requestScope.advert != null--%>
<%--                && requestScope.advert.categories.id == category.id}">${category.name}</c:if>"--%>
        <div class="form-check form-switch">
            <label for="isActive" class="form-label">Do you want to publish this advert?</label>
            <input class="form-check-input ch-success" name="isActive" type="checkbox" id="isActive"
                   checked="<c:if test="${requestScope.advert != null}">${requestScope.advert.active}</c:if>">
        </div>
        <div class="btn-edit">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
</div>
</body>
</html>
