function deleteUser(id) {
    var result = confirm('Do you want to delete user?');
    if (result) {
        var f = document.form;
        f.method = "post";
        f.action = '/delete?id=' + id;
        f.submit();
    } else {
        return false;
    }
}

function ban(id) {
    var result = confirm('Do you want to ban user?');
    if (result) {
        var f = document.form;
        f.method = "post";
        f.action = '/ban?id=' + id;
        f.submit();
    } else {
        return false;
    }
}

function removeBan(id) {
    var result = confirm('Do you want to remove ban user?');
    if (result) {
        var f = document.form;
        f.method = "post";
        f.action = '/remove-ban?id=' + id;
        f.submit();
    } else {
        return false;
    }
}