const patternOnlyLetters = /^[A-Za-z]+$/;

function validateRegistration() {
    return isNameValid() && isSurnameValid() && isEmailValid() && isDobValid() && isPasswordsValid()
}

function isNameValid() {
    const firstname = document.getElementById("firstName").value;
    const errorFirstname = document.getElementById("errorFirstName");

    var isValid = false;

    if (!patternOnlyLetters.test(firstname)) {
        errorFirstname.innerHTML = "Enter name without symbols and numbers";
        isValid = false;
    } else if (firstname === "") {
        errorFirstname.innerHTML = "Firstname shouldn't be empty";
        isValid = false;
    } else {
        errorFirstname.innerHTML = "";
        isValid = true;
    }
    return isValid;

}

function isSurnameValid() {
    const lastname = document.getElementById("lastName").value;
    const errorLastName = document.getElementById("errorLastName");
    var isValid = false;
    if (!patternOnlyLetters.test(lastname)) {
        errorLastName.innerHTML = "Enter name without symbols and numbers";
        isValid = false;
    } else if (lastname === "") {
        errorLastName.innerHTML = "Firstname and lastname shouldn't be empty";
        isValid = false;
    } else {
        errorLastName.innerHTML = "";
        isValid = true;
    }
    return isValid;
}

function isEmailValid() {
    const email = document.getElementById("emailRegistration").value;
    const errorEmail = document.getElementById("errorEmail");
    const patternIsEmail = /\S+@\S+\.\S+/;
    var isValid = false;
    if (email === "") {
        errorEmail.innerHTML = "Email shouldn't be empty";
        isValid = false;
    } else if (!patternIsEmail.test(email)) {
        errorEmail.innerHTML = "Email should have @ and be like <i>(example@mail.com)<i>"
        isValid = false;
    } else {
        errorEmail.innerHTML = "";
        isValid = true;
    }
    return isValid;
}

function isDobValid() {
    const dob = document.getElementById("dob").value;
    const errorDob = document.getElementById("errorDob")
    var isValid = false;
    /*let age = new Date.now() - new Date.parse(dob);//todo get past
    if (age < 0) {
        errorDob.innerHTML = "You should choose past";
        isValid = false;
    } else */
    if (dob === "") {
        errorDob.innerHTML = "Date of birthday shouldn't be empty";
        isValid = false;
    } else {
        errorDob.innerHTML = "";
        isValid = true;
    }
    return isValid;
}

function isPasswordsValid() {
    const password = document.getElementById("password").value;
    const passwordRepeat = document.getElementById("repeatPassword").value;
    const errorPassword = document.getElementById("errorPassword");
    const errorRepeatedPassword = document.getElementById("errorPasswordRepeat");
    var isValid = false;
    if (password === "") {
        errorPassword.innerHTML = "Field password empty!";
        isValid = false;
    } else if (password.length < 6 || password.length > 15) {
        errorPassword.innerHTML = "Password must be < 6 and > 15!";
        isValid = false;
    } else if (password !== passwordRepeat) {
        errorRepeatedPassword.innerHTML = "Passwords are different!";
        errorPassword.innerHTML = "";
        isValid = false;
    } else {
        errorPassword.innerHTML = "";
        errorRepeatedPassword.innerHTML = "";
        isValid = true;
    }
    return isValid;
}