const patternOnlyLetters = /^[A-Za-z]+$/;

function validateEditUser() {
    return isNameValid() && isSurnameValid() && isPasswordsValid()
}

function isNameValid() {
    const firstname = document.getElementById("firstNameEdit").value;
    const errorFirstname = document.getElementById("errorFirstName");

    var isValid = false;

    if (!patternOnlyLetters.test(firstname)) {
        errorFirstname.innerHTML = "Enter name without symbols and numbers";
        isValid = false;
    } else if (firstname === "") {
        errorFirstname.innerHTML = "Firstname shouldn't be empty";
        isValid = false;
    } else {
        errorFirstname.innerHTML = "";
        isValid = true;
    }
    return isValid;

}

function isSurnameValid() {
    const lastname = document.getElementById("inputLastName").value;
    const errorLastName = document.getElementById("errorLastName");
    var isValid = false;
    if (!patternOnlyLetters.test(lastname)) {
        errorLastName.innerHTML = "Enter name without symbols and numbers";
        isValid = false;
    } else if (lastname === "") {
        errorLastName.innerHTML = "Firstname and lastname shouldn't be empty";
        isValid = false;
    } else {
        errorLastName.innerHTML = "";
        isValid = true;
    }
    return isValid;
}

function isPasswordsValid() {
    const password = document.getElementById("inputPassword").value;
    const passwordRepeat = document.getElementById("repeatPassword").value;
    const errorPassword = document.getElementById("errorPassword");
    const errorRepeatedPassword = document.getElementById("errorPasswordRepeat");
    var isValid = false;
    if (password === "") {
        errorPassword.innerHTML = "Field password empty!";
        isValid = false;
    } else if (password.length < 6 || password.length > 15) {
        errorPassword.innerHTML = "Password must be < 6 and > 15!";
        isValid = false;
    } else if (password !== passwordRepeat) {
        errorRepeatedPassword.innerHTML = "Passwords are different!";
        errorPassword.innerHTML = "";
        isValid = false;
    } else {
        errorPassword.innerHTML = "";
        errorRepeatedPassword.innerHTML = "";
        isValid = true;
    }
    return isValid;
}