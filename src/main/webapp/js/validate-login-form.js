function isValidateEmail() {
    const email = document.getElementById("inputEmailLogin").value;
    const errorEmail = document.getElementById("errorEmail");
    const patternIsEmail = /\S+@\S+\.\S+/;
    var isValidate = false;
    if (email === "") {
        errorEmail.innerHTML = "Email shouldn't be empty";
        isValidate = false;
    } else if (!patternIsEmail.test(email)) {
        errorEmail.innerHTML = "Email should have @ and be like <i>(example@mail.com)<i>"
        isValidate = false;
    } else {
        errorEmail.innerHTML = "";
        isValidate = true;
    }
    return isValidate;
}

function isValidatePassword() {
    var isValidate = false;
    const password = document.getElementById("inputPasswordLogin").value;
    const errorPassword = document.getElementById("errorPassword");

    if (password === "") {
        errorPassword.innerHTML = "Field password empty!";
        isValidate = false;
    } else if (password.length < 5 || password.length > 15) {
        errorPassword.innerHTML = "Password must be < 5 and > 15!";
        isValidate = false;
    } else {
        errorPassword.innerHTML = "";
        isValidate = true;
    }
    return isValidate;
}

function isCorrectEmailAndPassword() {
    const errorLoginOrPassword = document.getElementById("errorLoginOrPassword").value;
    return errorLoginOrPassword === "";
}

function validateLoginForm() {
    return isValidateEmail() && isValidatePassword() && isCorrectEmailAndPassword();
}
