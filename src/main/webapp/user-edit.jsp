<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="uk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
            integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
            integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/user.css">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
            integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
            integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/user-edit.css">
    <style>
        .error-firstName, .error-lastname, .error-password, .error-password-repeat {
            color: #c80b0b;
            font-size: 12px;
            align-items: start;
            padding-top: 10px;
        }

        .container {
            width: 300px;
            display: flex;
            justify-content: center;
            height: 100%;
        }

        .btn-edit {
            margin: 10px;
            display: flex;
            justify-content: center;
        }

        .label-edit {
            margin-top: 100px;
        }

        .error-password, .error-firstName, .error-lastname, .error-password-repeat {
            display: flex;
            justify-content: left;
        }
    </style>
    <script>
        const patternOnlyLetters = /^[A-Za-z]+$/;

        function validateEditUser() {
            return isNameValid() && isSurnameValid() && isPasswordsValid()
        }

        function isNameValid() {
            const firstname = document.getElementById("firstNameEdit").value;
            const errorFirstname = document.getElementById("errorFirstName");

            var isValid = false;

            if (!patternOnlyLetters.test(firstname)) {
                errorFirstname.innerHTML = "Enter name without symbols and numbers";
                isValid = false;
            } else if (firstname === "") {
                errorFirstname.innerHTML = "Firstname shouldn't be empty";
                isValid = false;
            } else {
                errorFirstname.innerHTML = "";
                isValid = true;
            }
            return isValid;

        }

        function isSurnameValid() {
            const lastname = document.getElementById("inputLastName").value;
            const errorLastName = document.getElementById("errorLastName");
            var isValid = false;
            if (!patternOnlyLetters.test(lastname)) {
                errorLastName.innerHTML = "Enter name without symbols and numbers";
                isValid = false;
            } else if (lastname === "") {
                errorLastName.innerHTML = "Firstname and lastname shouldn't be empty";
                isValid = false;
            } else {
                errorLastName.innerHTML = "";
                isValid = true;
            }
            return isValid;
        }

        function isPasswordsValid() {
            const password = document.getElementById("inputPassword").value;
            const passwordRepeat = document.getElementById("repeatPassword").value;
            const errorPassword = document.getElementById("errorPassword");
            const errorRepeatedPassword = document.getElementById("errorPasswordRepeat");
            var isValid = false;
            if (password === "") {
                errorPassword.innerHTML = "Field password empty!";
                isValid = false;
            } else if (password.length < 6 || password.length > 15) {
                errorPassword.innerHTML = "Password must be < 6 and > 15!";
                isValid = false;
            } else if (password !== passwordRepeat) {
                errorRepeatedPassword.innerHTML = "Passwords are different!";
                errorPassword.innerHTML = "";
                isValid = false;
            } else {
                errorPassword.innerHTML = "";
                errorRepeatedPassword.innerHTML = "";
                isValid = true;
            }
            return isValid;
        }
    </script>
    <title>Edit profile</title>
</head>
<body>
<div class="edit-container">
    <div class="form-edit">
        <div class="container">
            <form action="users" method="post" onsubmit="return validateEditUser();">
                <input type="hidden" name="id" value="${requestScope.user.id}">
                <div class="form-group">
                    <h1 class="label-edit">Edit profile</h1>
                    <label for="firstNameEdit" class="form-label">Please, enter your first name</label>
                    <input type="text" class="form-control" id="firstNameEdit" name="firstname"
                           value="${requestScope.user.firstname}">
                    <span class="error-firstName" id="errorFirstName"></span>
                </div>
                <div class="form-group">
                    <label for="inputLastName" class="form-label">Please, enter your last name</label>
                    <input type="text" class="form-control" id="inputLastName" name="lastname"
                           value="${requestScope.user.lastname}">
                    <span class="error-lastname" id="errorLastName"></span>
                </div>
                <div class="form-group">
                    <label for="emailRegistration" class="form-label">Please, enter your email address</label>
                    <input type="email" class="form-control" id="emailRegistration" name="email"
                    value="${requestScope.user.email}">
                    <span class="error-email" id="errorEmail"></span>
                </div>
                <c:if test="${requestScope.user != null && requestScope.user.role == 'ADMIN'}">
                    <label for="role">Role: </label>
                    <select name="role" id="role">
                        <option value="USER">USER</option>
                        <option value="ADMIN">ADMIN</option>
                    </select>
                </c:if>
                <div class="form-group">
                    <label for="inputPassword" class="form-label">Enter password</label>
                    <input type="password" class="form-control" id="inputPassword" name="password" value="">
                    <span class="error-password" id="errorPassword"></span>
                </div>
                <div class="form-group">
                    <label for="repeatPassword" class="form-la bel">Repeat password</label>
                    <input type="password" class="form-control" id="repeatPassword" name="">
                    <span class="error-password-repeat" id="errorPasswordRepeat"></span>
                </div>
                <div class="btn-edit">
                    <button type="submit" class="btn btn-success">Update user</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
