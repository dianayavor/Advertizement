package server.service;

import server.model.Advert;
import server.model.Role;
import server.repository.AdvertRepository;
import server.repository.AdvertRepositoryImpl;
import server.repository.CrudRepository;

import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.util.List;

import static server.config.table.TableAdverts.TABLE_ADVERTS_FIELD_DATE_OF_CREATION;
import static server.config.table.TableAdverts.TABLE_ADVERTS_FIELD_DATE_OF_END;
import static server.model.Role.GUEST;
import static server.model.Role.USER;

public class AdvertService {
    private final ValidatorService<Advert> validatorService = new ValidatorService<>();
    private final CrudRepository<Advert> advertCrudRepository = new AdvertRepositoryImpl();
    private final AdvertRepository advertRepository = new AdvertRepositoryImpl();

    public Advert save(Advert advert) throws ValidationException {
        if (validatorService.validateEntity(advert).isEmpty()) {
            return advertCrudRepository.save(advert);
        } else return advert;
    }

    public Advert update(Advert advert) throws ValidationException {
        if (validatorService.validateEntity(advert).isEmpty()) {
            return advertCrudRepository.update(advert);
        } else return advert;
    }

    public boolean delete(@NotNull Long id) throws ValidationException {
        if (validatorService.validateId(id).isEmpty()) {
            return advertCrudRepository.delete(id);
        } else return false;
    }

    public Advert findById(@NotNull Long id) throws ValidationException {
        if (validatorService.validateId(id).isEmpty()) {
            return advertCrudRepository.findById(id);
        }
        return new Advert(id);
    }

    public List<Advert> findAll(Role role) {
        List<Advert> adverts = advertRepository.orderedByDate(TABLE_ADVERTS_FIELD_DATE_OF_CREATION, TABLE_ADVERTS_FIELD_DATE_OF_END, false);

        if (role == GUEST || role == USER) {
            adverts.removeIf(advert -> !advert.getActive());
        }
        return adverts;
    }
}
