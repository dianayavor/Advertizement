package server.service;

import server.model.User;
import server.repository.UserRepository;
import server.repository.UserRepositoryImpl;

import javax.validation.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class ValidatorService<E extends Serializable> {
    private final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private final Validator validator = factory.getValidator();

    public Set<ValidationException> validateEntity(E e) {
        Set<ConstraintViolation<E>> violations = validator.validate(e);
        Set<ValidationException> exceptions = new HashSet<>();
        for (ConstraintViolation<E> violation : violations) {
            exceptions.add(new ValidationException(violation.getInvalidValue() + ""));
        }
        return exceptions;
    }

    public Set<ValidationException> validateId(long id) {
        Set<ConstraintViolation<Long>> violations = validator.validate(id);
        Set<ValidationException> exceptions = new HashSet<>();
        for (ConstraintViolation<Long> violation : violations) {
            exceptions.add(new ValidationException(violation.getInvalidValue() + ""));
        }
        return exceptions;
    }

    public Set<ValidationException> validateEmail(String email) {
        Set<ConstraintViolation<String>> violations = validator.validate(email);
        Set<ValidationException> exceptions = new HashSet<>();
        for (ConstraintViolation<String> violation : violations) {
            exceptions.add(new ValidationException(violation.getInvalidValue() + ""));
        }
        return exceptions;
    }

    public ValidationException isEmailUnique(String email) {
        UserRepository repository = new UserRepositoryImpl();
        User user = repository.findByLogin(email);
        if (user.getId() != null) {
            return new ValidationException("This email already exists!");
        }
        return null;
    }
}
