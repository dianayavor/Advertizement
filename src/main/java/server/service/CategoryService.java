package server.service;

import server.model.Category;
import server.repository.CategoryRepository;
import server.repository.CrudRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CategoryService {
    private final ValidatorService<Category> validatorService = new ValidatorService<>();
    private final CrudRepository<Category> categoryCrudRepository = new CategoryRepository();

    public Category save(Category category) {
        if (validatorService.validateEntity(category).isEmpty()) {
            return categoryCrudRepository.save(category);
        } else return category;
    }

    public Category update(Category category) {
        if (validatorService.validateEntity(category).isEmpty()) {
            return categoryCrudRepository.update(category);
        } else return category;
    }

    public boolean delete(@NotNull long id) {
        if (validatorService.validateId(id).isEmpty()) {
            return categoryCrudRepository.delete(id);
        } else return false;
    }

    public Category findById(@NotNull long id) {
        if (validatorService.validateId(id).isEmpty()) {
            return categoryCrudRepository.findById(id);
        } else return new Category(id);
    }

    public List<Category> findAll() {
        return categoryCrudRepository.findAll();
    }
}
