package server.service;

import server.model.Advert;
import server.model.User;
import server.repository.CrudRepository;
import server.repository.UserRepository;
import server.repository.UserRepositoryImpl;

import javax.validation.ValidationException;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class UserService {
    private final ValidatorService<User> validatorService = new ValidatorService<>();
    private final CrudRepository<User> crudUserRepository = new UserRepositoryImpl();
    private final UserRepository userRepository = new UserRepositoryImpl();

    public User save(User user) throws ValidationException {
        if (validatorService.validateEntity(user).isEmpty() || validatorService.isEmailUnique(user.getEmail()) == null) {
            return crudUserRepository.save(user);
        } else return user;
    }

    public User update(User user) throws ValidationException {
        if (validatorService.validateEntity(user).isEmpty() || validatorService.isEmailUnique(user.getEmail()) == null) {
            return crudUserRepository.update(user);
        } else return user;
    }

    public boolean delete(@NotNull Long id) throws ValidationException {
        if (validatorService.validateId(id).isEmpty()) {
            return crudUserRepository.delete(id);
        } else return false;
    }

    public User findById(@NotNull Long id) throws ValidationException {
        if (validatorService.validateId(id).isEmpty()) {
            return crudUserRepository.findById(id);
        } else return new User(id);
    }

    public List<User> findAll() {
        return crudUserRepository.findAll();
    }

    public User findByLogin(@NotNull @Email String email) throws ValidationException {
        if (validatorService.validateEmail(email).isEmpty()) {
            return userRepository.findByLogin(email);
        } else {
            User user = new User();
            user.setEmail(email);
            return user;
        }
    }

    public List<Advert> findAllByUserId(@NotNull Long id) throws ValidationException {
        if (validatorService.validateId(id).isEmpty()) {
            return userRepository.findAllByUserId(id);
        }
        return new ArrayList<>();
    }

    public long findIdByLogin(@NotNull @Email String email) {
        if (validatorService.validateEmail(email).isEmpty()) {
            return userRepository.findIdByLogin(email);
        } else {
            return 0;
        }
    }

    public boolean changeUserStatus(boolean isActive, long id) {
        return userRepository.changeUserStatus(isActive, id);
    }
}
