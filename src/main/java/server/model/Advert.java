package server.model;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Advert implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String title;

    private String description;

    private User author;

    @PastOrPresent
    private LocalDate dateOfCreation;

    @Future
    private LocalDate dateOfEnd;

    private Category category;

    private Boolean isActive;

    public Advert() {
    }

    public Advert(@NotNull long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public LocalDate getDateOfEnd() {
        return dateOfEnd;
    }

    public void setDateOfEnd(LocalDate dateOfEnd) {
        this.dateOfEnd = dateOfEnd;
    }

    public Category getCategories() {
        return category;
    }

    public void setCategories(Category category) {
        this.category = category;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "Advert{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author=" + author +
                ", dateOfCreation=" + dateOfCreation +
                ", dateOfEnd=" + dateOfEnd +
                ", heading=" + category +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Advert)) return false;
        Advert advert = (Advert) o;
        return id.equals(advert.id) && title.equals(advert.title) && description.equals(advert.description) && author.equals(advert.author) && dateOfCreation.equals(advert.dateOfCreation) && dateOfEnd.equals(advert.dateOfEnd) && category == advert.category && isActive.equals(advert.isActive);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, author, dateOfCreation, dateOfEnd, category, isActive);
    }
}