package server.config.table;

import static server.config.table.Util.*;
import static server.repository.CrudRepository.*;

public class TableCategories {
    public static final String TABLE_NAME_CATEGORIES = "categories";
    public static final String TABLE_CATEGORIES_FIELD_ID = "id";
    public static final String TABLE_CATEGORIES_FIELD_NAME = "name";

    private TableCategories() {
    }

    public static boolean createTableCategories() {
        String query = SQL_QUERY_PATTERN_CREATE_TABLE_IF_EXISTS +
                TABLE_NAME_CATEGORIES + SQL_QUERY_PATTERN_OPEN_BRACKET +
                TABLE_CATEGORIES_FIELD_ID + SQL_QUERY_PATTERN_SERIAL_PRIMARY_KEY + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_CATEGORIES_FIELD_NAME + SQL_QUERY_PATTERN_TEXT + SQL_QUERY_PATTERN_CLOSE_BRACKET;
        return executeUpdate(query);
    }

    public static boolean deleteTableCategories() {
        String query = SQL_QUERY_PATTERN_DROP_TABLE_IF_EXISTS + TABLE_NAME_CATEGORIES;
        return executeUpdate(query);
    }
}
