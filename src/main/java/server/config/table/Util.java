package server.config.table;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Statement;

import static server.config.ConfigConnection.getConnection;

public class Util {
    private final static Logger logger = LogManager.getLogger(Util.class);

    final static String SQL_QUERY_PATTERN_CREATE_TABLE_IF_EXISTS = "create table if not exists ";
    final static String SQL_QUERY_PATTERN_DROP_TABLE_IF_EXISTS = "drop table if exists ";
    final static String SQL_QUERY_PATTERN_SERIAL_PRIMARY_KEY = " serial primary key";
    final static String SQL_QUERY_PATTERN_INT_REFERENCES_USERS = " int references users (" + TableAdverts.TABLE_ADVERTS_FIELD_ID + ") on delete cascade on update cascade";
    final static String SQL_QUERY_PATTERN_INT_REFERENCES_CATEGORIES = " int references categories (" + TableAdverts.TABLE_ADVERTS_FIELD_ID + ") on delete cascade on update cascade";
    final static String SQL_QUERY_PATTERN_DATE = " date";
    final static String SQL_QUERY_PATTERN_BOOLEAN = " boolean";
    final static String SQL_QUERY_PATTERN_TEXT = " text";
    final static String SQL_QUERY_PATTERN_VARCHAR = " varchar(20)";
    final static String SQL_QUERY_PATTERN_CASCADE = " cascade";

    private Util() {

    }

    public static boolean executeUpdate(String query) {
        try (Statement statement = getConnection().createStatement()) {
            statement.executeUpdate(query);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }
}
