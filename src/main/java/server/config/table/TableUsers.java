package server.config.table;

import static server.config.table.Util.*;
import static server.repository.CrudRepository.*;

public class TableUsers {
    public static final String TABLE_NAME_USERS = "users";

    public static final String TABLE_USERS_FIELD_ID = "id";
    public static final String TABLE_USERS_FIELD_FIRSTNAME = "firstname";
    public static final String TABLE_USERS_FIELD_LASTNAME = "lastname";
    public static final String TABLE_USERS_FIELD_EMAIL = "email";
    public static final String TABLE_USERS_FIELD_DATE_OF_BIRTH = "date_of_birth";
    public static final String TABLE_USERS_FIELD_IS_ACTIVE_ACCOUNT = "is_active_account";
    public static final String TABLE_USERS_FIELD_ROLE = "role";
    public static final String TABLE_USERS_FIELD_PASSWORD = "password";

    private TableUsers() {
    }

    public static boolean createTableUsers() {
        String queryBuilder = SQL_QUERY_PATTERN_CREATE_TABLE_IF_EXISTS +
                TABLE_NAME_USERS + SQL_QUERY_PATTERN_OPEN_BRACKET +
                TABLE_USERS_FIELD_ID + SQL_QUERY_PATTERN_SERIAL_PRIMARY_KEY + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_FIRSTNAME + SQL_QUERY_PATTERN_TEXT + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_LASTNAME + SQL_QUERY_PATTERN_TEXT + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_EMAIL + SQL_QUERY_PATTERN_TEXT + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_DATE_OF_BIRTH + SQL_QUERY_PATTERN_DATE + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_IS_ACTIVE_ACCOUNT + SQL_QUERY_PATTERN_BOOLEAN + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_PASSWORD + SQL_QUERY_PATTERN_TEXT + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_ROLE + SQL_QUERY_PATTERN_VARCHAR +
                SQL_QUERY_PATTERN_CLOSE_BRACKET;
        return executeUpdate(queryBuilder);
    }

    public static boolean deleteTableUsers() {
        String query = SQL_QUERY_PATTERN_DROP_TABLE_IF_EXISTS +
                TABLE_NAME_USERS +
                SQL_QUERY_PATTERN_CASCADE;
        return executeUpdate(query);
    }
}
