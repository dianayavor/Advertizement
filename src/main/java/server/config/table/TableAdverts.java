package server.config.table;

import static server.config.table.Util.*;
import static server.repository.CrudRepository.*;

public class TableAdverts {
    public static final String TABLE_NAME_ADVERTS = " adverts ";

    public static final String TABLE_ADVERTS_FIELD_ID = "id";
    public static final String TABLE_ADVERTS_FIELD_TITLE = "title";
    public static final String TABLE_ADVERTS_FIELD_DESCRIPTION = "description";
    public static final String TABLE_ADVERTS_FIELD_AUTHOR = "author_id";
    public static final String TABLE_ADVERTS_FIELD_DATE_OF_CREATION = "date_of_creation";
    public static final String TABLE_ADVERTS_FIELD_DATE_OF_END = "date_of_end";
    public static final String TABLE_ADVERTS_FIELD_CATEGORY = "category_id";
    public static final String TABLE_ADVERTS_FIELD_IS_ACTIVE = "is_active";

    private TableAdverts() {
    }

    public static boolean createTableAdverts() {
        String query = SQL_QUERY_PATTERN_CREATE_TABLE_IF_EXISTS +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_OPEN_BRACKET +
                TABLE_ADVERTS_FIELD_ID + SQL_QUERY_PATTERN_SERIAL_PRIMARY_KEY + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_TITLE + SQL_QUERY_PATTERN_TEXT + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_DESCRIPTION + SQL_QUERY_PATTERN_TEXT + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_AUTHOR + SQL_QUERY_PATTERN_INT_REFERENCES_USERS + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_DATE_OF_CREATION + SQL_QUERY_PATTERN_DATE + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_DATE_OF_END + SQL_QUERY_PATTERN_DATE + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_CATEGORY + SQL_QUERY_PATTERN_INT_REFERENCES_CATEGORIES + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_IS_ACTIVE + SQL_QUERY_PATTERN_BOOLEAN + SQL_QUERY_PATTERN_CLOSE_BRACKET;
        return executeUpdate(query);
    }

    public static boolean deleteTableAdverts() {
        String query = SQL_QUERY_PATTERN_DROP_TABLE_IF_EXISTS + TABLE_NAME_ADVERTS;
        return executeUpdate(query);
    }
}
