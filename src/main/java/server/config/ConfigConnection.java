package server.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.Driver;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConfigConnection {
    private final static Logger logger = LogManager.getLogger(ConfigConnection.class);
    private final static String PATH = "D:/webappadv/Advertisment/src/main/resources/db.properties";

    private ConfigConnection() {}

    public static Connection getConnection() {
        Connection connection = null;
        Properties properties = getProperties();
        try {
            DriverManager.registerDriver(new Driver());
            String localhost = properties.getProperty("localhost");
            String username = properties.getProperty("username");
            String password = properties.getProperty("password");
            connection = DriverManager.getConnection(localhost, username, password);
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return connection;
    }

    private static Properties getProperties() {
        Properties properties = new Properties();
        try (FileInputStream inputStream = new FileInputStream(PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return properties;
    }
}
