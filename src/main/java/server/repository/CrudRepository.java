package server.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.model.Advert;
import server.model.Category;
import server.model.Role;
import server.model.User;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static server.config.ConfigConnection.getConnection;
import static server.config.table.TableAdverts.*;
import static server.config.table.TableUsers.*;

public interface CrudRepository<E extends Serializable> {
    String SQL_QUERY_PATTERN_SELECT = "select ";
    String SQL_QUERY_PATTERN_FROM = " from ";
    String SQL_QUERY_PATTERN_SELECT_ALL_FROM = "select * from ";
    String SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_ADVERTS = "select * from adverts ";
    String SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_USERS = "select * from users ";
    String SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_CATEGORIES = "select * from categories ";

    String SQL_QUERY_PATTERN_INSERT_INTO = "insert into ";
    String SQL_QUERY_PATTERN_UPDATE = "update ";
    String SQL_QUERY_PATTERN_DELETE_FROM = "delete from ";
    String SQL_QUERY_PATTERN_VALUES_7 = ") values(?, ?, ?, ?, ?, ?, ?) ";
    String SQL_QUERY_PATTERN_VALUES_1 = ") values(?) ";

    String SQL_QUERY_PATTERN_SET = " set ";
    String SQL_QUERY_PATTERN_WHERE_ID_EQUALS_Q = " where id=? ";
    String SQL_QUERY_PATTERN_WHERE = " where ";
    String SQL_QUERY_PATTERN_ASC = " asc ";
    String SQL_QUERY_PATTERN_DESC = " desc ";
    String SQL_QUERY_PATTERN_ORDER_BY = " order by ";
    String SQL_QUERY_PATTERN_ON = " on ";
    String SQL_QUERY_PATTERN_AND = " and ";
    String SQL_QUERY_PATTERN_JOIN = " join ";
    String SQL_QUERY_PATTERN_INNER_JOIN = " inner join ";

    String SQL_QUERY_PATTERN_SEPARATOR_COMMA = ", ";
    String SQL_QUERY_PATTERN_SEPARATOR_POINT = ".";
    String SQL_QUERY_PATTERN_SEPARATOR_QUOTE = "'";
    String SQL_QUERY_PATTERN_EQUALS_Q = "=? ";
    String SQL_QUERY_PATTERN_EQUALS = " = ";
    String SQL_QUERY_PATTERN_OPEN_BRACKET = "(";
    String SQL_QUERY_PATTERN_CLOSE_BRACKET = ");";

    Logger logger = LogManager.getLogger(CrudRepository.class);

    E save(E e);

    E update(E e);

    boolean delete(Long id);

    E findById(Long id);

    List<E> findAll();

    E getObject(ResultSet resultSet);

    default Advert getMappedAdvert(ResultSet result) {
        Advert advert = new Advert();
        try {
            advert.setId((long) result.getInt(TABLE_ADVERTS_FIELD_ID));
            advert.setTitle(result.getString(TABLE_ADVERTS_FIELD_TITLE));
            advert.setDescription(result.getString(TABLE_ADVERTS_FIELD_DESCRIPTION));
            CategoryRepository categoryRepository = new CategoryRepository();
            long id = Long.parseLong(result.getString(TABLE_ADVERTS_FIELD_CATEGORY));
            Category category = categoryRepository.findById(id);//todo
            advert.setCategories(category);
            advert.setActive(result.getBoolean(TABLE_ADVERTS_FIELD_IS_ACTIVE));
            advert.setDateOfCreation(result.getDate(TABLE_ADVERTS_FIELD_DATE_OF_CREATION).toLocalDate());
            advert.setDateOfEnd(result.getDate(TABLE_ADVERTS_FIELD_DATE_OF_END).toLocalDate());
            advert.setAuthor(getMappedUser(result));
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return advert;
    }

    default User getMappedUser(ResultSet result) {
        User user = new User();
        try {
            user.setId((long) result.getInt(TABLE_USERS_FIELD_ID));
            user.setFirstname(result.getString(TABLE_USERS_FIELD_FIRSTNAME));
            user.setLastname(result.getString(TABLE_USERS_FIELD_LASTNAME));
            user.setEmail(result.getString(TABLE_USERS_FIELD_EMAIL));
            user.setDateOfBirth(result.getDate(TABLE_USERS_FIELD_DATE_OF_BIRTH).toLocalDate());
            user.setRole(Role.valueOf(result.getString(TABLE_USERS_FIELD_ROLE)));
            user.setActiveAccount(result.getBoolean(TABLE_USERS_FIELD_IS_ACTIVE_ACCOUNT));
            user.setPassword(result.getString(TABLE_USERS_FIELD_PASSWORD));
            user.setAdverts(new ArrayList<>());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    default List<E> getObjects(String query) {
        List<E> entities = new ArrayList<>();
        try (ResultSet result = getConnection().createStatement().executeQuery(query)) {
            while (result.next()) {
                entities.add(getObject(result));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entities;
    }

}
