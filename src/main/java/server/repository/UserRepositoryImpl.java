package server.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.model.Advert;
import server.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static server.config.ConfigConnection.getConnection;
import static server.config.table.TableAdverts.*;
import static server.config.table.TableUsers.*;

public class UserRepositoryImpl implements UserRepository, CrudRepository<User> {
    private final Logger logger = LogManager.getLogger(UserRepositoryImpl.class);

    @Override
    public User save(User user) {
        String query = SQL_QUERY_PATTERN_INSERT_INTO + TABLE_NAME_USERS + SQL_QUERY_PATTERN_OPEN_BRACKET +
                TABLE_USERS_FIELD_FIRSTNAME + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_LASTNAME + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_EMAIL + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_DATE_OF_BIRTH + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_ROLE + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_IS_ACTIVE_ACCOUNT + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_PASSWORD +
                SQL_QUERY_PATTERN_VALUES_7;
        try (PreparedStatement ps = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, user.getFirstname());
            ps.setString(2, user.getLastname());
            ps.setString(3, user.getEmail());
            ps.setDate(4, Date.valueOf(user.getDateOfBirth()));
            ps.setString(5, user.getRole().toString());
            ps.setBoolean(6, user.isActiveAccount());
            ps.setString(7, user.getPassword());
            ps.executeUpdate();
            ResultSet result = ps.getGeneratedKeys();
            while (result.next()) {
                user.setId((long) result.getInt(1));
            }
            result.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    @Override
    public User update(User user) {
        String query = SQL_QUERY_PATTERN_UPDATE + TABLE_NAME_USERS + SQL_QUERY_PATTERN_SET +
                TABLE_USERS_FIELD_FIRSTNAME + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_LASTNAME + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_EMAIL + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_ROLE + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_IS_ACTIVE_ACCOUNT + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_USERS_FIELD_PASSWORD + SQL_QUERY_PATTERN_EQUALS_Q +
                SQL_QUERY_PATTERN_WHERE_ID_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setString(1, user.getFirstname());
            ps.setString(2, user.getLastname());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getRole().toString());
            ps.setBoolean(5, user.isActiveAccount());
            ps.setString(6, user.getPassword());
            ps.setLong(7, user.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    @Override
    public boolean delete(Long id) {
        String query = SQL_QUERY_PATTERN_DELETE_FROM + TABLE_NAME_USERS + SQL_QUERY_PATTERN_WHERE_ID_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public User findById(Long id) {
        User user = new User();
        String query = SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_USERS + SQL_QUERY_PATTERN_WHERE_ID_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                user = getMappedUser(result);
            }
            result.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        return getObjects(SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_USERS);
    }

    @Override
    public User getObject(ResultSet result) {
        return getMappedUser(result);
    }

    public User findByLogin(String login) {
        User user = new User();
        String query = SQL_QUERY_PATTERN_SELECT_ALL_FROM + TABLE_NAME_USERS +
                SQL_QUERY_PATTERN_WHERE +
                TABLE_USERS_FIELD_EMAIL + SQL_QUERY_PATTERN_EQUALS +
                SQL_QUERY_PATTERN_SEPARATOR_QUOTE + login + SQL_QUERY_PATTERN_SEPARATOR_QUOTE;
        try (ResultSet result = getConnection().createStatement().executeQuery(query)) {
            while (result.next()) {
                user = getObject(result);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    public List<Advert> findAllByUserId(Long id) {
        Set<Advert> adverts = new HashSet<>();
        String query = SQL_QUERY_PATTERN_SELECT +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_ID + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_TITLE + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_DESCRIPTION + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_DATE_OF_CREATION + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_AUTHOR + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_DATE_OF_END + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_CATEGORY + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_IS_ACTIVE +
                SQL_QUERY_PATTERN_FROM + TABLE_NAME_ADVERTS +
                SQL_QUERY_PATTERN_INNER_JOIN + TABLE_NAME_USERS + SQL_QUERY_PATTERN_ON +
                TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SEPARATOR_POINT + TABLE_ADVERTS_FIELD_AUTHOR + SQL_QUERY_PATTERN_EQUALS +
                id +
                SQL_QUERY_PATTERN_WHERE + TABLE_ADVERTS_FIELD_DATE_OF_END + " > now() " + SQL_QUERY_PATTERN_ORDER_BY +
                TABLE_ADVERTS_FIELD_DATE_OF_CREATION + SQL_QUERY_PATTERN_DESC;
        try (ResultSet result = getConnection().createStatement().executeQuery(query)) {
            while (result.next()) {
                adverts.add(getMappedAdvert(result));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ArrayList<>(adverts);
    }

    @Override
    public long findIdByLogin(String login) {
        String query = SQL_QUERY_PATTERN_SELECT + TABLE_USERS_FIELD_ID + SQL_QUERY_PATTERN_FROM
                + TABLE_NAME_USERS + SQL_QUERY_PATTERN_WHERE + TABLE_USERS_FIELD_EMAIL +
                SQL_QUERY_PATTERN_EQUALS_Q;
        long id = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setString(1, login);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                id = result.getInt(TABLE_USERS_FIELD_ID);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return id;
    }

    @Override
    public boolean changeUserStatus(boolean isActive, long id) {
        String query = SQL_QUERY_PATTERN_UPDATE + TABLE_NAME_USERS + SQL_QUERY_PATTERN_SET
                + TABLE_USERS_FIELD_IS_ACTIVE_ACCOUNT + SQL_QUERY_PATTERN_EQUALS_Q
                + SQL_QUERY_PATTERN_WHERE_ID_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setBoolean(1, isActive);
            ps.setLong(2, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return false;
    }

}
