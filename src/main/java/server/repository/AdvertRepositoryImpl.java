package server.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.model.Advert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static server.config.ConfigConnection.getConnection;
import static server.config.table.TableAdverts.*;

public class AdvertRepositoryImpl implements CrudRepository<Advert>, AdvertRepository {
    private final Logger logger = LogManager.getLogger(AdvertRepositoryImpl.class);

    @Override
    public Advert save(Advert advert) {
        String query = SQL_QUERY_PATTERN_INSERT_INTO + TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_OPEN_BRACKET +
                TABLE_ADVERTS_FIELD_TITLE + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_DESCRIPTION + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_CATEGORY + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_DATE_OF_CREATION + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_DATE_OF_END + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_IS_ACTIVE + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_AUTHOR +
                SQL_QUERY_PATTERN_VALUES_7;
        try (PreparedStatement ps = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, advert.getTitle());
            ps.setString(2, advert.getDescription());
            ps.setLong(3, advert.getCategories().getId());
            ps.setDate(4, Date.valueOf(advert.getDateOfCreation()));
            ps.setDate(5, Date.valueOf(advert.getDateOfEnd()));
            ps.setBoolean(6, advert.getActive());
            ps.setLong(7, advert.getAuthor().getId());
            ps.executeUpdate();
            ResultSet result = ps.getGeneratedKeys();
            while (result.next()) {
                advert.setId((long) result.getInt(1));
            }
            result.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return advert;
    }

    @Override
    public Advert update(Advert advert) {
        String query = SQL_QUERY_PATTERN_UPDATE + TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_SET +
                TABLE_ADVERTS_FIELD_TITLE + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_DESCRIPTION + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_CATEGORY + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_SEPARATOR_COMMA +
                TABLE_ADVERTS_FIELD_IS_ACTIVE + SQL_QUERY_PATTERN_EQUALS_Q +
                SQL_QUERY_PATTERN_WHERE_ID_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setString(1, advert.getTitle());
            ps.setString(2, advert.getDescription());
            ps.setLong(3, advert.getCategories().getId());
            ps.setBoolean(4, advert.getActive());
            ps.setLong(5, advert.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return advert;
    }

    @Override
    public boolean delete(Long id) {
        String query = SQL_QUERY_PATTERN_DELETE_FROM + TABLE_NAME_ADVERTS + SQL_QUERY_PATTERN_WHERE_ID_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Advert findById(Long id) {
        String query = SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_ADVERTS + SQL_QUERY_PATTERN_WHERE_ID_EQUALS_Q;
        Advert advert = new Advert();
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                advert = getMappedAdvert(result);
            }
            result.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return advert;
    }

    @Override
    public List<Advert> findAll() {
        return getObjects(SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_ADVERTS);
    }

    @Override
    public Advert getObject(ResultSet result) {
        return getMappedAdvert(result);
    }

    @Override
    public List<Advert> orderedByDate(String dateCreation, String dateExpiration, boolean isAsc) {
        StringBuilder query = new StringBuilder();
        query.append(SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_ADVERTS)
                .append(SQL_QUERY_PATTERN_WHERE).append(dateExpiration).append(" > ")
                .append(" now() ")
                .append(SQL_QUERY_PATTERN_ORDER_BY).append(dateCreation);
        if (isAsc) {
            query.append(SQL_QUERY_PATTERN_ASC);
        } else {
            query.append(SQL_QUERY_PATTERN_DESC);

        }
        return new ArrayList<>(getObjects(query.toString()));
    }

}

