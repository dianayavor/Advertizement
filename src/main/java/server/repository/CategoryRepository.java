package server.repository;

import server.model.Category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static server.config.ConfigConnection.getConnection;
import static server.config.table.TableCategories.*;

public class CategoryRepository implements CrudRepository<Category> {
    @Override
    public Category save(Category category) {
        String query = SQL_QUERY_PATTERN_INSERT_INTO + TABLE_NAME_CATEGORIES +
                SQL_QUERY_PATTERN_OPEN_BRACKET + TABLE_CATEGORIES_FIELD_NAME +
                SQL_QUERY_PATTERN_VALUES_1;
        try (PreparedStatement ps = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, category.getName());
            ps.executeUpdate();
            ResultSet result = ps.getGeneratedKeys();
            while (result.next()) {
                category.setId(result.getInt(1));
            }
            result.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return category;
    }

    @Override
    public Category update(Category category) {
        String query = SQL_QUERY_PATTERN_UPDATE + TABLE_NAME_CATEGORIES + SQL_QUERY_PATTERN_SET +
                TABLE_CATEGORIES_FIELD_NAME + SQL_QUERY_PATTERN_EQUALS_Q + SQL_QUERY_PATTERN_WHERE +
                TABLE_CATEGORIES_FIELD_ID + SQL_QUERY_PATTERN_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setString(1, category.getName());
            ps.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return category;
    }

    @Override
    public boolean delete(Long id) {
        String query = SQL_QUERY_PATTERN_DELETE_FROM + TABLE_NAME_CATEGORIES + SQL_QUERY_PATTERN_WHERE +
                TABLE_CATEGORIES_FIELD_ID + SQL_QUERY_PATTERN_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setLong(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public Category findById(Long id) {
        Category category = new Category();
        String query = SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_CATEGORIES + SQL_QUERY_PATTERN_WHERE +
                TABLE_CATEGORIES_FIELD_ID + SQL_QUERY_PATTERN_EQUALS_Q;
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                category = getObject(result);
            }
            result.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return category;
    }

    @Override
    public List<Category> findAll() {
        return getObjects(SQL_QUERY_PATTERN_SELECT_ALL_FROM_TABLE_CATEGORIES);
    }

    @Override
    public Category getObject(ResultSet resultSet) {
        Category category = new Category();
        try {
            category.setId(resultSet.getInt(TABLE_CATEGORIES_FIELD_ID));
            category.setName(resultSet.getString(TABLE_CATEGORIES_FIELD_NAME));
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return category;
    }
}
