package server.repository;

import server.model.Advert;

import java.util.List;

public interface AdvertRepository {
    List<Advert> orderedByDate(String dateCreation, String dateExpiration, boolean isAsc);
}
