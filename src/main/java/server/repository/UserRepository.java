package server.repository;

import server.model.Advert;
import server.model.User;

import java.util.List;

public interface UserRepository {
    User findByLogin(String login);

    List<Advert> findAllByUserId(Long id);

    long findIdByLogin(String login);

    boolean changeUserStatus(boolean isActive, long id);

}
