package ui.dto;

import java.io.Serializable;

public class CategoryDto implements Serializable {
    private long id;
    private String name;

    public CategoryDto(){}

    public CategoryDto(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
