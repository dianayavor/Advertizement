package ui.dto.mapper;

import server.model.Role;
import server.model.User;
import ui.dto.UserDto;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class UserMapperImpl implements UserMapper {
    @Override
    public User toUserEntity(UserDto userDto) {
        if (userDto == null) {
            return null;
        }

        User user = new User();

        user.setId(userDto.getId());
        user.setFirstname(userDto.getFirstname());
        user.setLastname(userDto.getLastname());
        user.setEmail(userDto.getEmail());
        if (userDto.getDateOfBirthday() != null) {
            user.setDateOfBirth(LocalDate.parse(userDto.getDateOfBirthday()));
        }
        user.setActiveAccount(userDto.isActive());
        if (userDto.getRole() != null) {
            user.setRole(Enum.valueOf(Role.class, userDto.getRole()));
        }
        user.setPassword(userDto.getPassword());

        return user;
    }

    @Override
    public UserDto toUserDto(User user) {
        if (user == null) {
            return null;
        }

        UserDto userDto = new UserDto();

        if (user.getId() != null) {
            userDto.setId(user.getId());
        }
        userDto.setFirstname(user.getFirstname());
        userDto.setLastname(user.getLastname());
        userDto.setEmail(user.getEmail());
        if (user.getDateOfBirth() != null) {
            userDto.setDateOfBirthday(user.getDateOfBirth().toString());
        }
        userDto.setActive(user.isActiveAccount());
        if (user.getRole() != null) {
            userDto.setRole(user.getRole().name());
        }
        userDto.setPassword(user.getPassword());

        return userDto;
    }

    @Override
    public List<UserDto> toUsersDto(List<User> users) {
        return users.stream().map(this::toUserDto).collect(Collectors.toList());
    }
}
