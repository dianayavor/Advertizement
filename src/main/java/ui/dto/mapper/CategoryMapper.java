package ui.dto.mapper;

import org.mapstruct.InheritInverseConfiguration;
import server.model.Category;
import ui.dto.CategoryDto;

import java.util.List;

public interface CategoryMapper {

    Category toCategoryEntity(CategoryDto categoryDto);

    @InheritInverseConfiguration
    CategoryDto toCategoryDto(Category categories);

    List<CategoryDto> toCategoryDto(List<Category> categories);
}
