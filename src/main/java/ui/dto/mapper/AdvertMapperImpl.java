package ui.dto.mapper;

import server.model.Advert;
import ui.dto.AdvertDto;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class AdvertMapperImpl implements AdvertMapper {
    private final CategoryMapper categoryMapper = new CategoryMapperImpl();
    private final UserMapper userMapper = new UserMapperImpl();

    @Override
    public Advert toAdvertEntity(AdvertDto advertDto) {
        if (advertDto == null) {
            return null;
        }

        Advert advert = new Advert();

        advert.setId(advertDto.getId());
        advert.setTitle(advertDto.getTitle());
        advert.setDescription(advertDto.getDescription());
        advert.setCategories(categoryMapper.toCategoryEntity(advertDto.getCategories()));
        advert.setActive(advertDto.isActive());
        if (advertDto.getDateOfCreation() != null) {
            advert.setDateOfCreation(LocalDate.parse(advertDto.getDateOfCreation()));
        }
        if (advertDto.getDateOfEnd() != null) {
            advert.setDateOfEnd(LocalDate.parse(advertDto.getDateOfEnd()));
        }
        advert.setAuthor(userMapper.toUserEntity(advertDto.getUserDto()));

        return advert;
    }

    @Override
    public AdvertDto toAdvertDto(Advert advert) {
        if (advert == null) {
            return null;
        }

        AdvertDto advertDto = new AdvertDto();

        advertDto.setId(advert.getId());
        advertDto.setTitle(advert.getTitle());
        advertDto.setDescription(advert.getDescription());
        advertDto.setCategories(categoryMapper.toCategoryDto(advert.getCategories()));
        if (advert.getActive() != null) {
            advertDto.setActive(advert.getActive());
        }
        if (advert.getDateOfCreation() != null) {
            advertDto.setDateOfCreation(advert.getDateOfCreation().toString());
        }
        if (advert.getDateOfEnd() != null) {
            advertDto.setDateOfEnd(advert.getDateOfEnd().toString());
        }

        advertDto.setUserDto(userMapper.toUserDto(advert.getAuthor()));

        return advertDto;
    }

    @Override
    public List<AdvertDto> toAdvertsDto(List<Advert> adverts) {
        return adverts.stream().map(this::toAdvertDto).collect(Collectors.toList());
    }


}
