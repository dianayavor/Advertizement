package ui.dto.mapper;

import server.model.Category;
import ui.dto.CategoryDto;

import java.util.List;
import java.util.stream.Collectors;

public class CategoryMapperImpl implements CategoryMapper {


    @Override
    public Category toCategoryEntity(CategoryDto categoryDto) {
        if (categoryDto == null) {
            return null;
        }
        Category category = new Category();

        category.setId(categoryDto.getId());
        category.setName(categoryDto.getName());

        return category;
    }

    @Override
    public CategoryDto toCategoryDto(Category category) {
        if (category == null) {
            return null;
        }

        CategoryDto categoryDto = new CategoryDto();

        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());

        return categoryDto;
    }

    @Override
    public List<CategoryDto> toCategoryDto(List<Category> categories) {
        return categories.stream().map(this::toCategoryDto).collect(Collectors.toList());
    }
}
