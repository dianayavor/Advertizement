package ui.dto.mapper;

import org.mapstruct.InheritInverseConfiguration;
import server.model.Advert;
import ui.dto.AdvertDto;

import java.util.List;

public interface AdvertMapper {
    Advert toAdvertEntity(AdvertDto advertDto);

    @InheritInverseConfiguration
    AdvertDto toAdvertDto(Advert advert);

    List<AdvertDto> toAdvertsDto(List<Advert> advert);
}
