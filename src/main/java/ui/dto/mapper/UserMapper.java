package ui.dto.mapper;

import org.mapstruct.InheritInverseConfiguration;
import server.model.User;
import ui.dto.UserDto;

import java.util.List;

public interface UserMapper {

    User toUserEntity(UserDto userDto);

    @InheritInverseConfiguration
    UserDto toUserDto(User user);

    List<UserDto> toUsersDto(List<User> user);
}
