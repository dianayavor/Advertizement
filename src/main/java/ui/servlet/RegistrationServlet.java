package ui.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.service.UserService;
import ui.dto.UserDto;
import ui.dto.mapper.UserMapper;
import ui.dto.mapper.UserMapperImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

import java.io.IOException;

import static ui.servlet.ServletUtil.*;
import static ui.servlet.security.PasswordUtils.hashPassword;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LogManager.getLogger(RegistrationServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        dispatcher(req, resp, "/registration-form.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        UserService userService = new UserService();
        UserMapper userMapper = new UserMapperImpl();
        UserDto user = getUserDtoFromPost(req);
        try {
            UserDto loadedUser = userMapper.toUserDto(userService.findByLogin(user.getEmail()));
            if (loadedUser.getId() == 0) {
                user.setPassword(hashPassword(user.getPassword()));
                userService.save(userMapper.toUserEntity(user));
                resp.sendRedirect(BASE_URL + "/login");
                req.setAttribute(ERROR, "");
            } else {
                req.setAttribute(ERROR, "User with such mail exists!");
                req.setAttribute(FIRSTNAME, user.getFirstname());
                req.setAttribute(LASTNAME, user.getFirstname());
                req.setAttribute(DOB, user.getDateOfBirthday());
                req.setAttribute(EMAIL, user.getEmail());
                doGet(req, resp);
            }
        } catch (ValidationException | IOException e) {
            logger.error(e.getMessage());
        }
    }
}
