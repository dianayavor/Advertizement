package ui.servlet.local.storage;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static ui.servlet.ServletUtil.TOKEN;

public class CookieUtil {

    private CookieUtil() {
    }

    public static void saveCookie(HttpServletResponse resp, String token) {
        Cookie cookie = new Cookie(TOKEN, token);
        cookie.setDomain("localhost");
        cookie.setPath("/webappadw_main_war_exploded");
        resp.addCookie(cookie);
    }

    public static String readCookie(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(TOKEN)) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static void deleteCookie(HttpServletRequest req, HttpServletResponse resp) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(TOKEN)) {
                    cookie.setValue("");
                    cookie.setMaxAge(0);
                    cookie.setDomain("localhost");
                    cookie.setPath("/webappadw_main_war_exploded");
                    resp.addCookie(cookie);
                }
            }
        }
    }

}
