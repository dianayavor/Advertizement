package ui.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.service.UserService;
import ui.dto.UserDto;
import ui.dto.mapper.UserMapper;
import ui.dto.mapper.UserMapperImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import static ui.servlet.ServletUtil.*;

@WebServlet(urlPatterns = {"/users", "/users/delete", "/users/edit", "/users/ban", "/users/remove-ban"})
public class UserServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(UserServlet.class);
    private final UserService userService = new UserService();
    private final UserMapper userMapper = new UserMapperImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        String action = req.getServletPath();
        switch (action) {
            case "/users/edit":
                try {
                    req.setAttribute("user", userMapper.toUserDto(userService.findById(getId(req))));
                    dispatcher(req, resp, "/user-edit.jsp");
                } catch (ValidationException e) {
                    logger.error(e.getMessage());
                }
                break;
            case "/users/delete":
                deleteUser(resp, req);
                break;
            case "/users/ban":
                bunUser(resp, getId(req));
                break;
            case "/users/remove-ban":
                removeBunUser(resp, getId(req));
                break;
            default:
                showUsers(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String action = req.getServletPath();
        if ("/users/edit".equals(action)) {
            updateUser(req);
        } else {
            showUsers(req, resp);
        }
    }

    private void showUsers(HttpServletRequest req, HttpServletResponse resp) {
        req.setAttribute("users", userMapper.toUsersDto(userService.findAll()));
        dispatcher(req, resp, "/users.jsp");
    }

    private void updateUser(HttpServletRequest req) {
        try {
            UserDto userDto = getUserDtoFromPost(req);
            if (userDto.getId() == 0) {
                userService.save(userMapper.toUserEntity(userDto));
            } else {
                userService.update(userMapper.toUserEntity(userDto));
            }
        } catch (ValidationException e) {
            logger.error(e.getMessage());
        }
    }

    private void deleteUser(HttpServletResponse resp, HttpServletRequest req) {
        userService.delete(getId(req));
        req.setAttribute("users", userMapper.toUsersDto(userService.findAll()));
        redirectToUsers(resp);
    }

    private long getId(HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();
        long id = 0;
        if (params.containsKey("id")) {
            id = Long.parseLong(removeBraces(Arrays.toString(params.get("id"))));
        }
        return id;
    }

    private void bunUser(HttpServletResponse resp, long id) {
        userService.changeUserStatus(false, id);
        redirectToUsers(resp);
    }

    private void removeBunUser(HttpServletResponse resp, long id) {
        userService.changeUserStatus(true, id);
        redirectToUsers(resp);
    }

    private void redirectToUsers(HttpServletResponse resp) {
        try {
            resp.sendRedirect(BASE_URL + "/users");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
