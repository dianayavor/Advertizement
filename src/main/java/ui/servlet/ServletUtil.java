package ui.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.model.Role;
import ui.dto.AdvertDto;
import ui.dto.CategoryDto;
import ui.dto.UserDto;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static ui.servlet.AdvertServlet.getUserFromCookies;

public class ServletUtil {
    public final static String BASE_URL = "/webappadw_main_war_exploded";
    private final static Logger logger = LogManager.getLogger(ServletUtil.class);
    public final static String TOKEN = "token";
    public final static String ERROR = "error";
    public final static String FIRSTNAME = "firstname";
    public final static String LASTNAME = "lastname";
    public final static String PASSWORD = "password";
    public final static String DOB = "dob";
    public final static String EMAIL = "email";
    public final static String ROLE = "role";

    private static int monthForActiveAdvert = 3;

    private ServletUtil() {
    }

    public static void dispatcher(HttpServletRequest req, HttpServletResponse resp, String path) {
        try {
            RequestDispatcher dispatcher = req.getRequestDispatcher(path);
            dispatcher.forward(req, resp);
        } catch (ServletException | IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static String removeBraces(String text) {
        return text.replace("[", "").replace("]", "");
    }

    public static UserDto getUserDtoFromPost(HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();

        UserDto userDto = new UserDto();

        if (params.containsKey("id"))
            userDto.setId(Long.parseLong(removeBraces(Arrays.toString(params.get("id")))));
        if (params.containsKey(FIRSTNAME)) {
            userDto.setFirstname(removeBraces(Arrays.toString(params.get(FIRSTNAME))));
        }
        if (params.containsKey(LASTNAME)) {
            userDto.setLastname(removeBraces(Arrays.toString(params.get(LASTNAME))));
        }
        if (params.containsKey(EMAIL)) {
            userDto.setEmail(removeBraces(Arrays.toString(params.get(EMAIL))));
        }
        if (params.containsKey(DOB)) {
            userDto.setDateOfBirthday(removeBraces(Arrays.toString(params.get(DOB))));
        }
        if (params.containsKey(PASSWORD)) {
            userDto.setPassword(removeBraces(Arrays.toString(params.get(PASSWORD))));
        }
        userDto.setActive(true);
        if (params.containsKey(ROLE)) {
            userDto.setRole(removeBraces(Arrays.toString(params.get(ROLE))));
        }
        if (userDto.getRole() == null) {
            userDto.setRole(Role.USER.name());
        }
        return userDto;
    }

    public static AdvertDto getAdvertFromPost(HttpServletRequest req, UserDto author) {
        Map<String, String[]> params = req.getParameterMap();

        AdvertDto advertDto = new AdvertDto();
        if (params.containsKey("advertId") && !removeBraces(Arrays.toString(params.get("advertId"))).equals("")) {
            advertDto.setId(Long.parseLong(removeBraces(Arrays.toString(params.get("advertId")))));
        }
        if (params.containsKey("title")) {
            advertDto.setTitle(removeBraces(Arrays.toString(params.get("title"))));
        }
        if (params.containsKey("description")) {
            advertDto.setDescription(removeBraces(Arrays.toString(params.get("description"))));
        }
        if (params.containsKey("category")) {
            advertDto.setCategories(new CategoryDto(Long.parseLong(removeBraces(Arrays.toString(params.get("category"))))));
        }
        if (params.containsKey("isActive")) {
            String status = removeBraces(Arrays.toString(params.get("isActive")));
            advertDto.setActive(status.equals("on"));
        }
        if (advertDto.getId() == 0) {
            advertDto.setDateOfCreation(LocalDate.now().toString());
            advertDto.setDateOfEnd(LocalDate.now().plusMonths(monthForActiveAdvert).toString());
        }
        advertDto.setUserDto(author);
        return advertDto;
    }

    public static Map<String, Object> userToJson(UserDto user) {
        Map<String, Object> map = new HashMap<>();
        map.put(ROLE, user.getRole());
        map.put(FIRSTNAME, user.getFirstname());
        map.put(LASTNAME, user.getLastname());
        map.put(EMAIL, user.getEmail());
        return map;
    }

    public static UserDto jsonToUser(String json) {
        if (!json.isEmpty()) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                return objectMapper.readValue(json, UserDto.class);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        return new UserDto();
    }

    public static String getUserRole(HttpServletRequest req) {
        UserDto userDto = getUserFromCookies(req);
        return userDto.getRole() == null ? Role.GUEST.name() : userDto.getRole();
    }
}
