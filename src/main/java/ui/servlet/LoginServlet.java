package ui.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.service.UserService;
import ui.dto.UserDto;
import ui.dto.mapper.UserMapper;
import ui.dto.mapper.UserMapperImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import static ui.servlet.ServletUtil.*;
import static ui.servlet.local.storage.CookieUtil.saveCookie;
import static ui.servlet.security.PasswordUtils.checkPassword;
import static ui.servlet.security.TokenUtil.createToken;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private final Logger logger = LogManager.getLogger();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        dispatcher(req, resp, "/login-form.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        final String path = BASE_URL + "/adverts";

        final UserService userService = new UserService();
        final UserMapper userMapper = new UserMapperImpl();

        Map<String, String[]> params = req.getParameterMap();
        String password = "";
        String username = "";
        if (params.containsKey("password")) {
            password = removeBraces(Arrays.toString(params.get("password")));
        }
        if (params.containsKey("username")) {
            username = removeBraces(Arrays.toString(params.get("username")));
        }

        try {
            UserDto user = userMapper.toUserDto(userService.findByLogin(username));
            if (user.getId() == 0) {
                req.setAttribute(ERROR, "User does nor exists!");
                logger.error("User does nor exists!");
                saveUserData(req, resp, username, password);
            } else if (!checkPassword(password, user.getPassword())) {
                req.setAttribute(ERROR, "Incorrect login or password");
                logger.error("Incorrect login or password");
                saveUserData(req, resp, username, password);
            } else if (!user.isActive()) {
                req.setAttribute(ERROR, "You account was banned");
                logger.error("You account was banned");
                saveUserData(req, resp, username, password);
            } else {
                saveCookie(resp, createToken(user));
                req.setAttribute(ERROR, "");
                req.setAttribute(EMAIL, "");
                req.setAttribute(PASSWORD, "");
                resp.sendRedirect(path);
            }
        } catch (IOException | ValidationException e) {
            logger.error(e.getMessage());
        }
    }

    private void saveUserData(HttpServletRequest req, HttpServletResponse resp, String username, String password) {
        req.setAttribute(EMAIL, username);
        req.setAttribute(PASSWORD, password);
        doGet(req, resp);
    }
}
