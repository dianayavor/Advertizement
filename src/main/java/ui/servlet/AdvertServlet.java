package ui.servlet;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import server.model.Role;
import server.service.AdvertService;
import server.service.CategoryService;
import server.service.UserService;
import ui.dto.AdvertDto;
import ui.dto.UserDto;
import ui.dto.mapper.*;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static ui.servlet.ServletUtil.*;
import static ui.servlet.local.storage.CookieUtil.deleteCookie;
import static ui.servlet.local.storage.CookieUtil.readCookie;
import static ui.servlet.security.TokenUtil.decodeToken;

@WebServlet(urlPatterns = {"/adverts", "/my-adverts", "/adverts/delete", "/adverts/edit", "/adverts/edited", "/adverts/add", "/adverts/logout"})
public class AdvertServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LogManager.getLogger(AdvertServlet.class);

    private final AdvertService advertService = new AdvertService();
    private final UserService userService = new UserService();
    private final CategoryService categoryService = new CategoryService();

    private Map<String, String[]> params;

    private final UserMapper userMapper = new UserMapperImpl();
    private final AdvertMapper advertMapper = new AdvertMapperImpl();
    private final CategoryMapper categoryMapper = new CategoryMapperImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        UserDto currentUser = getUserFromCookies(req);
        currentUser.setId(getUserId(req));
        if (currentUser.getEmail() == null) {
            currentUser.setRole(Role.GUEST.toString());
        }
        req.setAttribute("user", currentUser);
        params = req.getParameterMap();


        String action = req.getServletPath();
        switch (action) {
            case "/adverts/edit":
                addEditShowJsp(req, resp);
                break;
            case "/adverts/add":
                setCategories(req);
                dispatcher(req, resp, "/advert-add-edit.jsp");
                break;
            case "/adverts/delete":
                deleteAdvert(req, resp);
                break;
            case "/adverts/logout":
                logout(req, resp);
                break;
            default:
                showAdverts(req, resp);
                break;
        }
    }

    private void addEditShowJsp(HttpServletRequest req, HttpServletResponse resp) {
        try {
            req.setAttribute("advert", advertMapper.toAdvertDto(advertService.findById(getAdvertId())));
            setCategories(req);
            dispatcher(req, resp, "/advert-add-edit.jsp");
        } catch (ValidationException e) {
            req.setAttribute(ERROR, e.getMessage());
            logger.error(e.getMessage());
        }
    }

    private void deleteAdvert(HttpServletRequest req, HttpServletResponse resp) {
        try {
            advertService.delete(getAdvertId());
            redirectToAdverts(req, resp);
        } catch (ValidationException e) {
            logger.error(e.getMessage());
        }
    }

    private long getAdvertId() {
        long advertId = 0;
        if (params.containsKey("advertId")) {
            advertId = Long.parseLong(removeBraces(Arrays.toString(params.get("advertId"))));
        }
        return advertId;
    }

    private void logout(HttpServletRequest req, HttpServletResponse resp) {
        UserDto currentUser = new UserDto();
        currentUser.setRole(Role.GUEST.toString());
        req.setAttribute("user", currentUser);
        deleteCookie(req, resp);
        redirectToAdverts(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        params = req.getParameterMap();
        addEditAdvert(req);
        redirectToAdverts(req, resp);
    }

    private void addEditAdvert(HttpServletRequest req) {
        if ("/adverts/edit".equals(req.getServletPath()) || "/adverts/add".equals(req.getServletPath())) {
            try {
                AdvertDto advertDto = getAdvertFromPost(req, new UserDto(getUserId(req)));
                if (advertDto.getId() == 0) {
                    advertService.save(advertMapper.toAdvertEntity(advertDto));
                } else {
                    advertService.update(advertMapper.toAdvertEntity(advertDto));
                }
            } catch (ValidationException e) {
                req.setAttribute(ERROR, e.getMessage());
                logger.error(e.getMessage());
            }
        }
    }


    private void showAdverts(HttpServletRequest req, HttpServletResponse resp) {
        params = req.getParameterMap();

        String action = req.getServletPath();
        switch (action) {
            case "/my-adverts":
                showAdverts(req, resp, advertMapper.toAdvertsDto(userService.findAllByUserId(getUserId(req))));
                break;
            case "/adverts":
            default:
                showAdverts(req, resp, advertMapper.toAdvertsDto(advertService.findAll(Role.valueOf(getUserRole(req)))));
                break;

        }
    }

    private void showAdverts(HttpServletRequest req, HttpServletResponse resp, Collection<AdvertDto> advertsDto) {
        req.setAttribute("adverts", advertsDto);
        dispatcher(req, resp, "/adverts.jsp");
    }

    private void redirectToAdverts(HttpServletRequest req, HttpServletResponse resp) {
        try {
            req.setAttribute("adverts", advertMapper.toAdvertsDto(advertService.findAll(Role.valueOf(getUserRole(req)))));
            resp.sendRedirect(BASE_URL + "/adverts");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void setCategories(HttpServletRequest req) {
        req.setAttribute("categories", categoryMapper.toCategoryDto(categoryService.findAll()));
    }

    private UserDto getUserFromCookies(HttpServletRequest req) {
        UserDto currentUser = new UserDto();
        if (readCookie(req) != null) {
            String token = readCookie(req);
            try {
                if (token == null) {
                    return currentUser;
                } else {
                    String json = decodeToken(token);
                    currentUser = jsonToUser(json);
                }
            } catch (UnsupportedEncodingException e) {
                logger.error(e.getMessage());
            }
        }
        return currentUser;
    }

    private long getUserId(HttpServletRequest req) {
        String email = getUserFromCookies(req).getEmail();
        if (email != null && !email.isEmpty()) {
            return userService.findIdByLogin(email);
        }
        return 0;
    }

    private String getUserRole(HttpServletRequest req) {
        UserDto userDto = getUserFromCookies(req);
        return userDto.getRole() == null ? Role.GUEST.name() : userDto.getRole();
    }
}
