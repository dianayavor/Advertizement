package ui.servlet.filter;

import server.model.Role;
import ui.servlet.ServletUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static server.model.Role.GUEST;
import static server.model.Role.USER;
import static ui.servlet.ServletUtil.dispatcher;

@WebFilter("/users")
public class UsersFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Role role = Role.valueOf(ServletUtil.getUserRole((HttpServletRequest) request));
        if (role == GUEST || role == USER) {
            dispatcher((HttpServletRequest) request, (HttpServletResponse) response, "/not-found.jsp");
        }
        chain.doFilter(request, response);
    }

}
