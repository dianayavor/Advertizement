package ui.servlet.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.sql.Connection;

import static server.config.ConfigConnection.getConnection;

@WebFilter(urlPatterns = "/*")
public class ConnectionFilter implements Filter {
    private static final Logger logger = LogManager.getLogger(ConnectionFilter.class);
    private Connection connection = null;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        connection = getConnection();
        if (connection != null) {
            chain.doFilter(request, response);
        } else {
            logger.error("Connection to db lost!");
        }
    }
}
