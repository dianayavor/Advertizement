package ui.servlet.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.apache.tomcat.util.codec.binary.Base64;
import ui.dto.UserDto;

import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import static ui.servlet.ServletUtil.userToJson;

public class TokenUtil {
    private TokenUtil() {
    }

    public static String createToken(UserDto userDto) {
        SignatureAlgorithm algorithm = SignatureAlgorithm.HS256;
        SecretKey secretKey = MacProvider.generateKey(algorithm);
        Map<String, Object> claims = userToJson(userDto);
        return Jwts.builder()
                .setClaims(claims)
                .signWith(algorithm, secretKey)
                .compact();
    }

    public static String decodeToken(String token) throws UnsupportedEncodingException {
        if (!token.isEmpty()) {
            String payload = token.split("\\.")[1];
            return new String(Base64.decodeBase64(payload), "UTF-8");
        }
        return "";
    }

}
