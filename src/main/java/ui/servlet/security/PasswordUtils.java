package ui.servlet.security;

import org.mindrot.jbcrypt.BCrypt;

public class PasswordUtils {
    private final static int WORKLOAD = 12;

    private PasswordUtils() {
    }

    public static String hashPassword(String password) {
        String salt = BCrypt.gensalt(WORKLOAD);
        return BCrypt.hashpw(password, salt);
    }

    public static boolean checkPassword(String password, String storedHash) {
        return BCrypt.checkpw(password, storedHash);
    }
}
