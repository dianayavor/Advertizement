import server.config.table.TableAdverts;
import server.config.table.TableCategories;
import server.config.table.TableUsers;

public class Main {
    public static void main(String[] args) {
        TableUsers.createTableUsers();
        TableCategories.createTableCategories();
        TableAdverts.createTableAdverts();
    }
}
